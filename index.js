'use strict'

var Url = require('url-parse')

var nodeFetch = require('node-fetch')

function tinyGithubSdk() {
}

var apiUrl = 'https://api.github.com'
var fetchFn = function (url, options) {
    try {
        var _fetchFn = nodeFetch
        try {
            if (fetch) {//TODO: find correct way to check if defined
                _fetchFn = fetch
            }
        } catch (err) {
            _fetchFn = nodeFetch
        }
        return _fetchFn(url, options)
    } catch (err) {
        return Promise.reject(err)
    }
}

var getIssueInfo = function (owner, repo, issueId) {
    try {
        if (!owner)
            throw new Error('owner is undefined or null')
        if (!repo)
            throw new Error('repo is undefined or null')
        if (!issueId)
            throw new Error('issueId is undefined or null')

        var infoUrl = `${this.apiUrl}/repos/${owner}/${repo}/issues/${issueId}`
        return fetchFn(infoUrl)
            .then(res => res.json())
            .then(issueData => {
                issueData.owner = owner
                issueData.repo = repo
                return issueData
            })
    } catch (err) {
        return Promise.reject(err)
    }
}

var tryGetPullRequestsByCommit = function (commitId) {
    return fetchFn(`${this.apiUrl}/search/issues?q=${commitId} type:pr`)
        .then(res => res.json())
        .then(searchData => {
            var res = {res: false}
            if (searchData.incomplete_results) {//more then 100 results. Should raise collision event
                return res
            }
            var pullRequests = []
            for (var i = 0; i < searchData.items.length; i++) {
                if (searchData.items[i].pull_request)
                    pullRequests.push(
                        fetchFn(searchData.items[i].pull_request.url)
                            .then(res => {
                                if (res) {
                                    return res.json()
                                } else {
                                    return null
                                }
                            })
                    )
            }

            return Promise.all(pullRequests)
                .then(pullRequestData => {
                    res.res = true
                    var allPullRequests = []
                    for (var c = 0; c < pullRequestData.length; c++) {
                        if (pullRequestData[c])
                            allPullRequests = allPullRequests.concat(pullRequestData[c])
                    }
                    res.items = allPullRequests
                    return res
                })
        })
}

var tryGetResolver = function (owner, repo, issueId) {
    return this.getIssueInfo(owner, repo, issueId)
        .then(issueData => {
            if (issueData.state === 'closed') {
                var eventsUrl = `${this.apiUrl}/repos/${owner}/${repo}/issues/${issueId}/events`
                return fetchFn(eventsUrl)
            }
            return null
        })
        .then(res => {
            if (res) {
                return res.json()
            } else {
                return null
            }
        })
        .then(eventsData => {
            if (eventsData) {
                var closed = eventsData.filter(x => x.event === 'closed')
                var closedDate = null
                if (closed.length < 1)
                    return []
                closedDate = closed[closed.length - 1].created_at

                var referencedEvents = eventsData.filter(x => x.event === 'referenced'
                    && x.commit_id
                    && x.created_at >= closedDate)
                var commits = referencedEvents
                    .map(refEvent => refEvent.commit_id)
                    .filter((value, index, self) => value && self.indexOf(value) === index)
                return Promise.all(commits.map(commit_id => {
                    return this.tryGetPullRequestsByCommit(commit_id)
                }))
                    .then(pullRequests => {
                        var hasErrors = pullRequests.filter(r => !r.res)
                        if (hasErrors.length > 0)
                            return []
                        var allPullRequests = []
                        for (var i = 0; i < pullRequests.length; i++) {
                            allPullRequests = allPullRequests.concat(pullRequests[i].items)
                        }

                        var allMerged = allPullRequests.filter(pr => pr.merged)
                        if (allMerged.length != 1)
                            return []
                        return allMerged.map(pr => {
                            return pr.user && pr.user.id
                        })
                    })
            } else {
                return null
            }
        })
        .then(resolversData => {
            if (resolversData) {
                return {resolved: true, resolver: (resolversData.length == 1 ? resolversData[0] : null)}
            } else {
                return null
            }
        })
}

var getUsersByIds = function (userIds) {
    var requests = []
    var result = []
    for (var i = 0; i < userIds.length; i++) {
        var userUrl = `${this.apiUrl}/user/${userIds[i]}`
        requests.push(fetchFn(userUrl)
            .then(res => res.json())
            .then(userData => result.push(userData))
        )
    }
    return Promise.all(requests)
        .then(() => result)
}

var getUserProfile = function (oAuthToken) {
    try {
        if (!oAuthToken)
            throw new Error('oAuthToken is undefined or null')

        var profileUrl = `${this.apiUrl}/user?access_token=${oAuthToken}`
        return fetchFn(profileUrl)
            .then(res => res.json())
            .then(profileData => {
                return profileData
            })
    } catch (err) {
        return Promise.reject(err)
    }
}

var getUserProfileByName = function (userName) {
    var infoUrl = `${this.apiUrl}/users/${userName}`
    return fetchFn(infoUrl)
        .then(res => res.json())
}

tinyGithubSdk.prototype = {
    apiUrl: apiUrl,
    getIssueInfo: getIssueInfo,
    tryGetResolver: tryGetResolver,
    getUsersByIds: getUsersByIds,
    getUserProfile: getUserProfile,
    getUserProfileByName: getUserProfileByName,
    tryGetPullRequestsByCommit: tryGetPullRequestsByCommit
}

module.exports = new tinyGithubSdk()